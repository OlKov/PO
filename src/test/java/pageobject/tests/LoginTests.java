package pageobject.tests;

import org.testng.annotations.Test;
import static org.testng.Assert.assertTrue;

public class LoginTests extends BaseTest {


    private static String WRONG_CHARACTER = "ffff";
    private static String WRONG_LOGIN = "test@ukr.net";
    private static String WRONG_PASSWORD = "wwwwwwwwwww";
    private static String CORRECT_LOGIN = "qoflq@ukr.net";
    private static String CORRECT_PASSWORD = "qwerty1234_!";

    @Test (priority = 1)
    public void checkLoginFormWithEmptyCredentials() {
        getHomePage().waitForPageLoadComplete(50);
        getHomePage().clickOnLoginButton();
        getHomePage().waitVisibilityOfElement(30, getLoginPage().getLoginContainer());
        getLoginPage().isFieldUsernameVisible();
        getLoginPage().isFieldPasswordVisible();
        getLoginPage().inputUsername(WRONG_CHARACTER);
        getLoginPage().cleanUsernameField(WRONG_CHARACTER);
        getHomePage().waitVisibilityOfElement(30, getLoginPage().getLoginError());
        assertTrue(getLoginPage().isErrorMessageUsernameVisible());
        getLoginPage().inputPassword(WRONG_CHARACTER);
        getLoginPage().cleanPasswordField(WRONG_CHARACTER);
        getLoginPage().waitVisibilityOfElement(30, getLoginPage().getPasswordError());
        assertTrue(getLoginPage().isErrorMessagePasswordVisible());
    }

    @Test (priority = 2)
    public void checkLoginFormWithIncorrectCredentials() {
        getHomePage().waitForPageLoadComplete(50);
        getHomePage().clickOnLoginButton();
        getHomePage().waitVisibilityOfElement(30, getLoginPage().getLoginContainer());
        getLoginPage().isFieldUsernameVisible();
        getLoginPage().isFieldPasswordVisible();
        getLoginPage().inputUsername(WRONG_LOGIN);
        getLoginPage().inputPassword(WRONG_PASSWORD);
        getLoginPage().isButtonLoginVisible();
        getLoginPage().clickOnLoginButton();
        getLoginPage().waitVisibilityOfElement(30, getLoginPage().getErrorBanner());
        assertTrue(getLoginPage().isErrorBannerVisible());
    }

    @Test (priority = 3)
    public void checkLoginFormWithCorrectCredentials() {
        getHomePage().waitForPageLoadComplete(50);
        getHomePage().clickOnLoginButton();
        getHomePage().waitVisibilityOfElement(30, getLoginPage().getLoginContainer());
        getLoginPage().isFieldUsernameVisible();
        getLoginPage().isFieldPasswordVisible();
        getLoginPage().inputUsername(CORRECT_LOGIN);
        getLoginPage().inputPassword(CORRECT_PASSWORD);
        getLoginPage().isButtonLoginVisible();
        getLoginPage().clickOnLoginButton();
        getUserPage().waitVisibilityOfElement(30, getUserPage().getSection());
        assertTrue(getUserPage().isSectionVisible());
    }
}
