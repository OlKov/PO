package pageobject.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import static org.openqa.selenium.By.xpath;

public class LoginPage extends BasePage {

    private static final String FIELD_USERNAME = "//input[@data-testid='login-username']";
    private static final String FIELD_PASSWORD = "//input[@id='login-password']";
    private static final String ERROR_USERNAME = "//div[@id='username-error']";
    private static final String ERROR_PASSWORD = "//div[@id='password-error']";
    private static final String LOGIN_BUTTON = "//button[@id='login-button']";
    private static final String LOGIN_CONTAINER="//div[@data-testid='login-container']";
    private static final String ERROR_BANNER="//div[contains(@class,'encore-negative-set')]";


    public LoginPage(WebDriver driver) {
        super(driver);
    }

    public void inputUsername(String ch) {
        driver.findElement(xpath(FIELD_USERNAME)).sendKeys(ch);
    }

    public void cleanUsernameField(String ch) {
        for (int i=0;i<ch.length();i++) {
            driver.findElement(xpath(FIELD_USERNAME)).sendKeys(Keys.BACK_SPACE);
        }
    }

    public void cleanPasswordField(String ch) {
        for (int i=0;i<ch.length();i++) {
            driver.findElement(xpath(FIELD_PASSWORD)).sendKeys(Keys.BACK_SPACE);
        }
    }
    public void isButtonLoginVisible() {
        driver.findElement(xpath(LOGIN_BUTTON)).isDisplayed();
    }

    public void clickOnLoginButton() {
        driver.findElement(xpath(LOGIN_BUTTON)).click();
    }

    public void isFieldUsernameVisible() {
        driver.findElement(xpath(FIELD_USERNAME)).isDisplayed();
    }

    public void inputPassword(String ch) {
        driver.findElement(xpath(FIELD_PASSWORD)).sendKeys(ch);

    }

    public void isFieldPasswordVisible() {
        driver.findElement(xpath(FIELD_PASSWORD)).isDisplayed();
    }

    public boolean isErrorMessageUsernameVisible() {
        return driver.findElement(xpath(ERROR_USERNAME)).isDisplayed();
    }

    public boolean isErrorMessagePasswordVisible() {
        return driver.findElement(xpath(ERROR_PASSWORD)).isDisplayed();
    }

    public boolean isErrorBannerVisible() {
        return driver.findElement(xpath(ERROR_BANNER)).isDisplayed();
    }

    public By getLoginContainer() {
        return xpath(LOGIN_CONTAINER);
    }

    public By getLoginError() {
        return xpath(ERROR_USERNAME);
    }

    public By getPasswordError() {
        return xpath(ERROR_PASSWORD);
    }
    public By getErrorBanner() {
        return xpath(ERROR_BANNER);
    }
}
