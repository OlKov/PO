package pageobject.pages;

import org.openqa.selenium.WebDriver;

import static org.openqa.selenium.By.xpath;

public class HomePage extends BasePage {

    private static final String LOGIN_BUTTON = "//button[@data-testid='login-button']";

    public HomePage(WebDriver driver) {
        super(driver);
    }

    public void clickOnLoginButton() {
        driver.findElement(xpath(LOGIN_BUTTON)).click();
    }

}

