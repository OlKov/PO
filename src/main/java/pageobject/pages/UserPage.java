package pageobject.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import static org.openqa.selenium.By.xpath;

public class UserPage extends BasePage {


    private static final String SECTION = "//section[@data-testid='home-page']";


    public UserPage(WebDriver driver) {
        super(driver);
    }

    public boolean isSectionVisible() {
        return driver.findElement(xpath(SECTION)).isDisplayed();
    }

    public By getSection() {
        return xpath(SECTION);
    }


}
